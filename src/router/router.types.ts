import type { VueElement } from "vue";
import type { AppLayoutsEnum } from "@/layouts/layouts.types";
import type { AccessScopesEnum } from "@/modules/auth/auth.types";

declare module "vue-router" {
  interface RouteMeta {
    layout?: AppLayoutsEnum;
    layoutComponent?: VueElement;
    accessScopes?: AccessScopesEnum[];
  }
}

export enum RouteNamesEnum {
  home = "home",
  accessError = "accessError",
  books = "books",
  cars = "cars",
}
